package com.maciejsokol.zad1;

import java.util.Date;
import java.util.Map;

public class Invoice {
    private Map<String, MerchandiseInfo> merchandiseInfo;
    private Date dateOfSale;
    private Date paymentDeadlineInDays;
    private Date paymentDeadlineDate;
    private float netPrice;
    private float tax;
    private float grossPrice;
    private int invoiceId;

    public Map<String, MerchandiseInfo> getMerchandiseInfo() {
        return merchandiseInfo;
    }

    public void setMerchandiseInfo(Map<String, MerchandiseInfo> merchandiseInfo) {
        this.merchandiseInfo = merchandiseInfo;
    }

    public Date getDateOfSale() {
        return dateOfSale;
    }

    public void setDateOfSale(Date dateOfSale) {
        this.dateOfSale = dateOfSale;
    }

    public Date getPaymentDeadlineInDays() {
        return paymentDeadlineInDays;
    }

    public void setPaymentDeadlineInDays(Date paymentDeadlineInDays) {
        this.paymentDeadlineInDays = paymentDeadlineInDays;
    }

    public Date getPaymentDeadlineDate() {
        return paymentDeadlineDate;
    }

    public void setPaymentDeadlineDate(Date paymentDeadlineDate) {
        this.paymentDeadlineDate = paymentDeadlineDate;
    }

    public Float getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(Float netPrice) {
        this.netPrice = netPrice;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public Float getGrossPrice() {
        return this.getNetPrice() * (this.getTax());
    }

    public void setGrossPrice(Float grossPrice) {
        this.grossPrice = grossPrice;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }
    public MerchandiseInfo searchMerchandiseInfo(String merchandiseInfoName) {
        return this.merchandiseInfo.get(merchandiseInfoName);
    }
}
