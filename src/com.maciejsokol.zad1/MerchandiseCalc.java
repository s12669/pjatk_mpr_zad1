package com.maciejsokol.zad1;

public class MerchandiseCalc {
    private float unitNetPrice;
    private float unitGrossPrice;
    private int unitsNumber;
    private float fullNetPrice;
    private float fullGrossPrice;
    private float tax;

    public float getUnitNetPrice() {
        return unitNetPrice;
    }

    public void setUnitNetPrice(float unitNetPrice) {
        this.unitNetPrice = unitNetPrice;
    }

    public float getUnitGrossPrice() {
        return (this.getUnitNetPrice() * (this.getTax()));
    }

    public void setUnitGrossPrice(float unitGrossPrice) {
        this.unitGrossPrice = unitGrossPrice;
    }

    public int getUnitsNumber() {
        return unitsNumber;
    }

    public void setUnitsNumber(int unitsNumber) {
        this.unitsNumber = unitsNumber;
    }

    public float getFullNetPrice() {
        return this.getUnitNetPrice() * (this.getUnitsNumber());
    }

    public void setFullNetPrice(float fullNetPrice) {
        this.fullNetPrice = fullNetPrice;
    }

    public float getFullGrossPrice() {
        return this.getUnitGrossPrice() * (this.getUnitsNumber());
    }

    public void setFullGrossPrice(float fullGrossPrice) {
        this.fullGrossPrice = fullGrossPrice;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }
}
